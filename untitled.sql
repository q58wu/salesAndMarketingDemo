connect to cs348
drop table product
create table product( \
	model_number integer not null primary key, \
	manufacturer varchar(100) not null, \
	date_of_release date not null, \
	retail_price double not null, \
	num_stock integer not null \
)

drop table customer 
create table customer( \
	customerID integer not null primary key, \
	customername varchar(30) not null, \
	email varchar(60) not null, \
	address varchar(60) not null, \
	domestic_or_foreign char(8) not null \
	num_of_orders integer not null \
	check (domestic_or_foreign = 'domestic' or domestic_or_foreign = 'foreign') \
)

drop table purchase_order
create table purchase_order( \
	model_number integer not null references product, \
	customerID integer not null references customer(cid), \
	sold_price double not null, \
	 
	primary key (model_number)
)

drop table lens
create table lens( \
	lens_model_number integer not null primary key references product, \
	aperture_range_min double not null, \
	aperture_range_max double not null, \
	max_focal_length double not null, \
	min_focal_length double not null \
	check(aperture_range_max > aperture_range_min and max_focal_length > min_focal_length)\
)
drop table prime_lens
create table prime_lens(
	lens_model_number integer not null primary key references product,\
	aperture_range_min double not null, \
	aperture_range_max double not null, \
	max_focal_length double not null, \
	min_focal_length double not null \
	check(aperture_range_max > aperture_range_min and max_focal_length > min_focal_length)\
)

drop table camera
create table camera( \
	model_number integer not null primary key references product, \
	sensor_size double not null, \
	pixel_number integer not null \
	primary key (model_number)
)
drop table camera_lens_replacable

create table camera_lens_replacable(
	model_number integer not null primary key references camera,\
	first_feature_number integer not null,\
	second_feature_number integer not null,\
	check(first_feature_number = 0or first_feature_number = 1)\ -- 0 means no eletronic viewfinder
	check(second_feature_number = 0 or
		second_feature_number = 1 or
		second_feature_number = 2 or
		second_feature_number = 3)\ -- 0 means there is no secondary feature, 1 represents optical finder 2 represents through the lens optical finder 3 represents optical rangefinder.
)


drop table camera_lens_notrepaceable

create table camera_lens_notrepaceable(
	model_number integer not null primary key references camera,\
	first_feature_number integer not null,\
	second_feature_number integer not null,\

	max_focal_length double not null,\
	min_focal_length double not null,\
	aperture_range_min double not null,\
	aperture_range_max double not null,\

	check(first_feature_number = 0or first_feature_number = 1)\ -- 0 means no eletronic viewfinder
	check(second_feature_number = 0 or
		second_feature_number = 1 or
		second_feature_number = 2 or
		second_feature_number = 3)\ -- 0 means there is no secondary feature, 1 represents optical finder 2 represents through the lens optical finder 3 represents optical rangefinder.
)
drop table customer_evaluation(
	rating integer not null,\
	comment_on_product char(1000),\

	model_number integer not null references product,\
	primary key (model_number)\
)

---- 

下一道题

select from prime_lens where (max_focal_length == min_focal_length && aperture_range_max == aperture_range_min)


select from purchase_order where sold_price != null

select from camera where (first_feature_number == 0 or first_feature_number == 1)

select from camera where (second_feature_number == 1 or second_feature_number == 2 or second_feature_number == 3 or second_feature_number == 0)


select from customer_rating where (customer_rating == 5 or customer_rating == 4 or customer_rating == 3 or customer_rating == 2 customer_rating == 1)



